package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const Addr = ":8081"

func main() {
	http.HandleFunc("/movies", movieListHandler)
	log.Printf("Starting on port %s", Addr)
	log.Fatal(http.ListenAndServe(Addr, nil))
}

type Movie struct {
	ID          int       `json:"id"`
	Name        string    `json:"name"`
	Poster      string    `json:"poster"`
	MovieUrl    string    `json:"movie_url"`
	IsPaid      bool      `json:"is_paid"`
	ReleaseYear time.Time `json:"release_year"`
	Genre       string    `json:"genre"`
}

func timeMustParse(year string) time.Time {
	t, err := time.Parse("2006", year)
	if err != nil {
		panic(err)
	}
	return t
}

func movieListHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	mm := []Movie{
		{0, "Бойцовский клуб", "/static/posters/fightclub.jpg", "https://youtu.be/qtRKdVHc-cE", true, timeMustParse("1999"), "triller"},
		{1, "Крестный отец", "/static/posters/father.jpg", "https://youtu.be/ar1SHxgeZUc", false, timeMustParse("1988"), "drama"},
		{2, "Криминальное чтиво", "/static/posters/pulpfiction.jpg", "https://youtu.be/s7EdQ4FqbhY", true, timeMustParse("1996"), "comedy"},
	}
	var res []Movie
	for _, movie := range mm {
		genres := r.URL.Query().Get("genres")
		if genres != "" {
			for _, genre := range strings.Split(genres, ",") {
				if movie.Genre == genre {
					res = append(res, movie)
				}
			}
		}

	}

	for _, movie := range mm {
		ids := r.URL.Query().Get("ids")
		if ids != "" {
			for _, id := range strings.Split(ids, ",") {
				i, _ := strconv.Atoi(id)
				if movie.ID == i {
					res = append(res, movie)
				}
			}
		}

	}

	err := json.NewEncoder(w).Encode(res)
	if err != nil {
		log.Printf("Render response error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}

}
